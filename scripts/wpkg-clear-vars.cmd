@echo off
set WPKG_REG=HKLM\Software\WPKG\vars

::WPKG variables defined in the registry as set per group policy.  Allows for per department variables to be defined
for /f "tokens=1,2,3" %%a in ('reg query %WPKG_REG%') do (
    if /i "%%b"=="reg_sz" set %%a=
)

set WPKG

exit /b
