@echo off
set WPKG_REG=HKLM\Software\WPKG\vars

::WPKG variables defined in the registry as set per group policy.  Allows for per department variables to be defined
for /f "tokens=1,2,3" %%a in ('reg query %WPKG_REG%') do (
    if /i "%%b"=="reg_sz" set %%a=%%c
)

if /i "%WPKGJS%"=="" echo.The value of %%WPKGJS%% is blank, not allowed&exit /b
if /i "%WPKGJS64%"=="" echo.The value of %%WPKGJS64%% is blank, not allowed&exit /b
if /i "%WPKGINSTITUTION%"=="" echo.The value of %%WPKGINSTITUTION%% is blank, not allowed&exit /b
if /i "%WPKGSHAREBASE%"=="" echo.The value of %%WPKGSHAREBASE%% is blank, not allowed&exit /b
if /i "%WPKGSOFTWARE%"=="" echo.The value of %%WPKGSOFTWARE%% is blank, not allowed&exit /b
if /i "%WPKGSOFTWAREUSER%"=="" echo.The value of %%WPKGSOFTWAREUSER%% is blank, not allowed&exit /b
if /i "%WPKGSOFTWAREPASSWORD%"=="" echo.The value of %%WPKGSOFTWAREPASSWORD%% is blank, not allowed&exit /b
if /i "%WPKGLOGDIR%"=="" echo.The value of %%WPKGLOGDIR%% is blank, not allowed&exit /b

if NOT exist %WPKGLOGDIR% mkdir %WPKGLOGDIR%

net use * %WPKGSHAREBASE% /user:%WPKGSOFTWAREUSER% %WPKGSOFTWAREPASSWORD% >nul 2>&1
dir "%WPKGSHAREBASE%" >nul 2>&1
if ERRORLEVEL 1 echo.Failed to see IFS files, not allowed&exit /b

if exist "%programfiles(x86)%" (goto 64bit) else (goto 32bit)

:32bit
set ARCH=x32
set PROGRAMFILESWPKG=%programfiles%
cscript  //E:jscript  //nologo %WPKGJS% %*
goto end

:64bit
set ARCH=x64
set PROGRAMFILESWPKG=%programfiles(x86)%
cscript  //E:jscript  //nologo %WPKGJS64% %*
goto end

:end
::remove the WPKGSOFTWARE share
FOR /F "tokens=2" %%D IN ('net use ^| find "%WPKGSHAREBASE%"') DO net use %%D /d >nul 2>&1

::clear the variables set at the top of this scripts
for /f "tokens=1,2,3" %%a in ('reg query %WPKG_REG%') do (
    if /i "%%b"=="reg_sz" set %%a=
)

exit /b
