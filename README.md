Shared WPKGs in use by both Chemistry and Maths

Environment variables
=====================
We expect that you will have set the following environment variables:

1. WPKGJS  
Full UNC path to 32-bit wpkg.js
1. WPKGJS64  
Full UNC path to 64-bit wpkg64.js
1. WPKGINSTITUTION  
Agreed-upon code your institution (see below)
1. WPKGSOFTWARE  
Full UNC path to the central software repository.
1. WPKGSHAREBASE  
Full UNC path to base of the central share.
1. WPKGSOFTWAREUSER  
Username that should be used for connecting to WPKGSOFTWARE
1. WPKGSOFTWAREPASSWORD  
Password that should be used for connecting to WPKGSOFTWARE as WPKGSOFTWAREUSER
1. WPKGLOGDIR  
Directory in which software installer logs are to be kept

The included wpkg wrapper script will look in HKLM\SOFWARE\WPKG\vars and set environment variables for all keys of type REG_SZ, so you could e.g.

reg add HKLM\SOFTWARE\WPKG\vars /v WPKGINSTITUTION /t REG_SZ /d chemistry

Note: in general, the expectation is that environment variables with names starting "WPKG" are somehow used in this repository, e.g. in package definitions, helper scripts, etc.

Share location
==============
* `set WPKGSOFTWARE=\\ifs.ch.private.cam.ac.uk\deploy\software`
* `set WPKGSHAREBASE=\\ifs.ch.private.cam.ac.uk\deploy`

Institute codes
===============
See https://gitlab.developers.cam.ac.uk/ch/co/ucam_wpkg/-/wikis/Institute-codes

Repository layout
================

* packages: directory with multiple xml files, each of corresponds to an individual WPKG package definition
* packages\wpkg\tools: scripts which are called from wpkg definition (see https://gitlab.developers.cam.ac.uk/ch/co/ucam_wpkg/-/wikis/Helper-scripts-used-in-packages)
* miscscripts: example scripts, e.g. wrappers around wpkg.js which map drives and set environment variables (see https://gitlab.developers.cam.ac.uk/ch/co/ucam_wpkg/-/wikis/Misc-other-scripts)

* common-config: config files that are used in a wpkg for all institutions
* %WPKGINSTITUTION%-config: config files specific to a particlar institution.
