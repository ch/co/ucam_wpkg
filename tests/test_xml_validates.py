import glob
from util import *

xmllist = XMLList(Config().config)

validator = XSDValidator("../xsd/packages.xsd")


def test_xsd():
    """Ensure packages conform to packages.xsd"""

    for file_name in xmllist.getfiles("test_xsd"):
        yield check_file, file_name


def check_file(file_name):
    assert validator.validate(
        file_name
    ), f"{file_name} does not pass validate against packages.xsd"
