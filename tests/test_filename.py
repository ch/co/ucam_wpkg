import os
import lxml, lxml.etree
from util import *

config = Config().config
xmllist = XMLList(config)


def test_filename():
    """Ensure package ids match filenames"""

    for file_name in xmllist.getfiles("test_filename"):
        doc = lxml.etree.parse(file_name)
        package_id = doc.xpath("//package")[0].get("id")

        yield filename_matches_packageid, file_name, package_id


def filename_matches_packageid(file_name, package_id):
    assert (
        os.path.basename(file_name) == package_id + ".xml"
    ), f"package id {package_id} does not match {file_name}"
