from lxml import etree
from lxml.etree import DocumentInvalid
import lxml
import glob
import os
import yaml

# import pprint
# pp = pprint.PrettyPrinter(indent=2)


class XSDValidator:
    def __init__(self, xsd_path):
        xmlschema_doc = etree.parse(xsd_path)
        self.xmlschema = etree.XMLSchema(xmlschema_doc)

    def validate(self, xml_path):
        xml_doc = etree.parse(xml_path)
        try:
            self.xmlschema.assertValid(xml_doc)
        except lxml.etree.DocumentInvalid:
            print("Validation error(s):")
            for error in self.xmlschema.error_log:
                print("  Line {}: {}".format(error.line, error.message))
        return self.xmlschema.validate(xml_doc)


class XMLList:
    def __init__(self, config):
        self.config = config
        self.files = set(glob.glob(self.config["package_glob"]))

    def getfiles(self, testname):
        if (
            "tests_to_skip" in self.config.keys()
            and testname in self.config["tests_to_skip"].keys()
        ):
            files_to_skip = {
                os.path.join(self.config["package_dir"], x)
                for x in self.config["tests_to_skip"][testname] or []
            }
            return self.files - files_to_skip
        else:
            return self.files


class Config:
    def __init__(self):
        with open("config.yml", "r") as conffile:
            self.defaults = {}
            self.defaults["variable_names_to_skip"] = {}

            config = yaml.load(conffile, Loader=yaml.SafeLoader)
            self.config = {**self.defaults, **config}
