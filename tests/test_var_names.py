import glob
import os
import re
import lxml, lxml.etree
from util import *

config = Config().config
xmllist = XMLList(config)

default_var_names = config["repo_var_names"] + config["win_var_names"]

variable_re = re.compile("%(.*?)%")


def test_var_names():
    """Ensure variable names are chosen appropriately"""

    for file_name in xmllist.getfiles("test_var_names"):
        with open(file_name, "r") as file:
            data = file.read()

        lc_vars = [x.lower() for x in variable_re.findall(data)]
        doc = lxml.etree.parse(file_name)
        pkg_vars = doc.xpath("//variable")
        allowed_pkg_vars = [x.get("name") for x in pkg_vars]

        skipped_pkg_vars = config.get("variable_names_to_skip").get(
            os.path.basename(file_name), []
        )

        for var in set(lc_vars):
            yield var_name_is_valid, file_name, var
            yield var_name_is_allowed, var, default_var_names + allowed_pkg_vars + skipped_pkg_vars, file_name


def var_name_is_valid(filename, varname):
    # NB there are very few restrictions on var names imposed by Windows.
    # But for sanity, we restrict ourselves to what we consider to be a
    # "sensible" subset
    valid_name_re = re.compile("^[a-z0-9()_-]*$")
    assert valid_name_re.match(varname)


def var_name_is_allowed(varname, allowed_vars, file_name):
    assert varname in [
        x.lower() for x in allowed_vars
    ], f"variable named {varname} not permitted in {file_name}"
