import os
import lxml, lxml.etree
import re
from util import *

config = Config().config
xmllist = XMLList(config)


def test_revision_value():
    """Ensure the package revision starts with %version%"""
    for file_name in xmllist.getfiles("test_revision_value"):
        doc = lxml.etree.parse(file_name)
        revision = doc.xpath("//package")[0].get("revision")

        yield revision_pattern_check, file_name, revision


def revision_pattern_check(file_name, revision):
    pattern = re.compile("^%version%.*", re.IGNORECASE)
    assert pattern.match(
        revision
    ), f"revision value for {file_name} does not start %version%"
