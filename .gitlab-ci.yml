image: registry.gitlab.developers.cam.ac.uk/ch/co/wpkg/wpkg-ci-docker:latest

variables:
  GIT_DEPTH: 10
  FF_SCRIPT_SECTIONS: "true"

stages:
  - lint
  - cachecheck
  - functionalcheck

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'
      when: never # we can't push directly to master so skip CI...
    - when: always # ...but default to running CI otherwise

tests:
  stage: lint
  before_script:
    - export DEBIAN_FRONTEND=noninteractive
    - export LANG=en_GB.UTF-8
    - export LANGUAGE=en_GB:en
    - export LC_ALL=en_GB.UTF-8
  script:
    - nosetests3 -w tests/


# Now, which packages should we test for functionality?
# cache - replace this with a "generic package". See https://stackoverflow.com/questions/71220441/how-to-fetch-an-artifact-file-from-previous-successful-build

# DONE. As ALT suggests, split the cachecheck stage into 'has the XML changed?' and 'has it been a while since last success?' stages.
# Detect CI_PIPELINE_SOURCE=="schedule"

# Filter XML for packages without citest=broken attribute
buildtemplates:
  before_script:
    - echo -e "\e[0Ksection_start:`date +%s`:before_script[collapsed=true]\r\e[0KRunning before_script"
    - export LANG=en_GB.UTF-8
    - export LANGUAGE=en_GB:en
    - export LC_ALL=en_GB.UTF-8
    - eval $(ssh-agent -s)
    - if [ -f "$WPKG_XML_TEST_PRIVKEY" ] ; then chmod 400 "$WPKG_XML_TEST_PRIVKEY" ; ssh-add "$WPKG_XML_TEST_PRIVKEY" ; fi
    - if [ -f "$WPKG_XML_KNOWNHOSTS" ] ; then mkdir --mode=0700 ~/.ssh ; cp "$WPKG_XML_KNOWNHOSTS" ~/.ssh/known_hosts ; chmod 0644 ~/.ssh/known_hosts ; fi
    - ssh -l root wpkg-xml-test.ch.private.cam.ac.uk $CI_PROJECT_NAME $CI_COMMIT_BRANCH
    - echo -e "\e[0Ksection_end:`date +%s`:before_script\r\e[0K"
  stage: cachecheck
  artifacts:
    paths: 
      - pkgs.json
      - .gitlab-ci-generated.yml
  # A script to generate some CI config
  script:
    - |
      SIXMONTHS=$((6 * 24 * 3600))
      declare -a IDENTS
      curl -H "JOB-TOKEN: $CI_JOB_TOKEN" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/status/1.0.0/status.json -o status.json || rm -f status.json
      if ! [ -f status.json ] ; then
        mkdir -p cache
        touch status.json
      fi
      cat status.json
      for F in packages/*.xml ; do 
        # Ensure this is a real file
        if ! [ -L "$F" ] ; then
          if [ "$(xmllint -xpath 'string(//variable[@name="GIT_CITEST"]/@value)' $F)" != "broken" ] ; then
            IDENT=$(xmllint -xpath 'string(//package/@id)' $F)
            if [ null == "$((cat status.json || true )| jq -r ".[\"${IDENT}\"]")" ] ; then
              echo $IDENT from $F not in status.json
              IDENTS+=($IDENT)
            else
              if [ $CI_PIPELINE_SOURCE == "schedule" ] ; then
                LASTDATE="$((cat status.json || true) | jq -r ".[\"${IDENT}\"].testdate")"
                AGE=$(( $(date +%s)-${LASTDATE:-0}))
                if [ ${AGE} -gt ${SIXMONTHS} ] ; then
                  echo $IDENT not tested recently - last test ${LASTDATE}
                  IDENTS+=($IDENT)
                fi
              else 
                OLDSUM="$((cat status.json || true ) | jq -r ".[\"${IDENT}\"].md5" | tr '[:upper:]' '[:lower:]')"
                NEWSUM=$(cat $F | unix2dos | md5sum | awk ' { print $1 } ')
                if [ "$OLDSUM" != "$NEWSUM" ] ; then
                  echo $IDENT has changed - OLD $OLDSUM New $NEWSUM
                  IDENTS+=($IDENT)
                fi
              fi
            fi
          fi
        fi
      done
      echo '{"data":' >pkgs.json
      if [ ${#IDENTS[@]} -ne 0 ] ; then
        printf '%s\n' "${IDENTS[@]}" | head -50 | jq -R . | jq -s . >>pkgs.json
      else
        echo '[]' >>pkgs.json
      fi
      echo '}' >>pkgs.json
      jinja -d pkgs.json .gitlab-ci-template.yml.j2 >.gitlab-ci-generated.yml
      # Adjust script to output valid CI yaml, then call them following
      # https://gitlab.com/gitlab-org/project-templates/jsonnet
      # https://docs.gitlab.com/ee/ci/yaml/#trigger

trigger-dynamic:
  stage: functionalcheck
  needs: 
    - buildtemplates
  trigger:
    include: 
      - artifact: .gitlab-ci-generated.yml
        job: buildtemplates
    strategy: depend

