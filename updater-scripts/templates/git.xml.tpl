<?xml version="1.0" encoding="UTF-8"?>
<packages:packages xmlns:packages="http://www.wpkg.org/packages">

  <package id="git" name="git" revision="%version%.1" priority="60" reboot="false">
    <variable name="version" value="${VERSION}" />
    <check type="uninstall" condition="versiongreaterorequal" path="Git" value="%version%" />

    <install cmd='%WPKGSOFTWARE%\git\git-%version%-64-bit.exe /sp- /verysilent /suppressmsgboxes /norestart' />

    <upgrade include='remove' />
    <upgrade include='install' />

    <remove cmd='"%programfiles%\git\unins000.exe" /verysilent /suppressmsgboxes /sp-'><exit code="-1"/></remove>
  </package>

</packages:packages>
