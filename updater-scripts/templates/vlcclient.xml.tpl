<?xml version="1.0" encoding="UTF-8"?>
<packages:packages xmlns:packages="http://www.wpkg.org/packages">
<package id="vlcclient" name="VLC media player" revision="%version%.1" priority="40" reboot="false">
<variable name="version" value="$VERSION" />

<check type="uninstall" condition="versiongreaterorequal" path="VLC media player" value="%version%" />

  <install cmd='"%WPKGSOFTWARE%\vlc\vlc-%VERSION%-win64.exe" /S /NCRC' />

  <upgrade include="remove"/>
  <upgrade include="install"/>

  <remove cmd='cmd /c %WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Au_.exe' />
  <remove cmd='cmd /c if exist "%PROGRAMFILES%\VideoLAN\VLC\uninstall.exe" "%PROGRAMFILES%\VideoLAN\VLC\uninstall.exe" /S' />
  <remove cmd='cmd /c %WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Au_.exe' />

</package>
</packages:packages>
