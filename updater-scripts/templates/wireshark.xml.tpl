<?xml version="1.0" encoding="UTF-8"?>
<packages:packages xmlns:packages="http://www.wpkg.org/packages">

  <package id="wireshark" name="wireshark" revision="%version%.1" priority="0" reboot="false">
    <variable name="version" value="${VERSION}" />

    <!-- wireshark silent installer doesn't install winpcap because the free winpcap has
          had silent installation disabled. Thus we have to handle it ourselves -->

    <check type="uninstall" condition="versiongreaterorequal" path="Wireshark.*" value="%version%" />
    <depends package-id="winpcap" />

    <install cmd='%WPKGSOFTWARE%\wireshark\wireshark-%version%-x64.exe /S /desktopicon=no /quicklaunchicon=no' />

    <upgrade include='remove' />
    <upgrade include='install' />

    <remove cmd='cmd /c %WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Un_A.exe' />

    <!-- at some point wireshark renamed the uninstaller binary. We thus check for both variants -->
    <remove cmd='"%programfiles%\wireshark\uninstall.exe" /S'>
      <condition>
        <check type="file" condition="exists" path="%programfiles%\wireshark\uninstall.exe" />
      </condition>
    </remove>

    <remove cmd='"%programfiles%\wireshark\uninstall-wireshark.exe" /S'>
      <condition>
        <check type="file" condition="exists" path="%programfiles%\wireshark\uninstall-wireshark.exe" />
      </condition>
    </remove>

    <remove cmd='cmd /c %WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Un_A.exe' />
  </package>

</packages:packages>
