<?xml version="1.0" encoding="UTF-8"?>
<packages:packages xmlns:packages="http://www.wpkg.org/packages">

  <package id="thunderbird" name="Mozilla Thunderbird" revision="%version%.1" reboot="false" priority="81">

    <!-- NB: we've had problems with major version bumps which sometimes change the format of options we add in %WPKGINSTITUTION%-prefs.js.
    Thus, we tie that to a version, and please test carefully when upgrading -->

    <!-- https://ftp.mozilla.org/pub/thunderbird/releases/ -->

    <variable name="version" value="${VERSION}" />  <!-- For Maths, set version value in package id thunderbird_config_maths to the same as this VERSION -->
    <variable name="fileversion" value="${FILEVERSION}" />
    <variable name="prefsversion" value="${PREFSVERSION}" />

    <check type="uninstall" condition="versiongreaterorequal" path="Mozilla Thunderbird.*" value="%version%" />
    <chain package-id="thunderbird_config_maths">
      <condition>
        <check type="file" condition="exists" path="%WPKGSHAREBASE%\%WPKGINSTITUTION%-config\thunderbird\customise_thunderbird" />
      </condition>
    </chain>

    <!-- Should Thunderbird 64 bit already be installed remove it -->
    <install cmd='%WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Un_A.exe' />
    <install cmd='"%programfiles%\Mozilla Thunderbird\uninstall\helper.exe" /S' >
      <condition>
         <check type="file" condition="exists" path="%programfiles%\Mozilla Thunderbird\uninstall\helper.exe" />
      </condition>
    </install>
    <install cmd='%WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Un_A.exe' />
    <!-- remove any config -->
    <install cmd='cmd /c rmdir /s /q "%programfiles%\Mozilla Thunderbird\"' >
      <condition>
         <check type="file" condition="exists" path="%programfiles%\Mozilla Thunderbird\thunderbird.cfg" />
      </condition>
    </install>

    <!-- Installing 32 bit version while Add-on's all move to 64 bit -->
    <install cmd='"%WPKGSOFTWARE%\thunderbird\Thunderbird Setup %fileversion%.exe" -ms' />

    <!-- optional institution-specific tweaks -->
    <install cmd='cmd /c copy %WPKGSHAREBASE%\%WPKGINSTITUTION%-config\thunderbird\%WPKGINSTITUTION%-prefs.js-%prefsversion% "%programfiles(x86)%\mozilla thunderbird\defaults\pref\%WPKGINSTITUTION%-prefs.js" '>
      <condition>
         <check type="file" condition="exists" path="%WPKGSHAREBASE%\%WPKGINSTITUTION%-config\thunderbird\%WPKGINSTITUTION%-prefs.js-%prefsversion%" />
      </condition>
    </install>

    <install cmd='cmd /c copy "%WPKGSHAREBASE%\%WPKGINSTITUTION%-config\thunderbird\cam.ac.uk.xml" "%programfiles(x86)%\mozilla thunderbird\isp\cam.ac.uk.xml" '>
      <condition>
         <check type="file" condition="exists" path="%WPKGSHAREBASE%\%WPKGINSTITUTION%-config\thunderbird\cam.ac.uk.xml" />
      </condition>
    </install>

    <install cmd='%WPKGSOFTWARE%\wpkg\tools\copydir.cmd "%WPKGSHAREBASE%\%WPKGINSTITUTION%-config\thunderbird\dictionaries" "%programfiles(x86)%\mozilla thunderbird\dictionaries"' >
      <condition>
         <check type="file" condition="exists" path="%WPKGSHAREBASE%\%WPKGINSTITUTION%-config\thunderbird\dictionaries" />
      </condition>
    </install>

    <install cmd='reg add HKLM\SOFTWARE\Policies\Mozilla\Thunderbird /t reg_dword /v DisableAppUpdate /d 1 /f'/>

    <!-- although an in-place upgrade works, Thunderbird then insists on a reboot the next time one tries to start Thunderbird -->
    <upgrade cmd='taskkill /F /IM thunderbird.exe'>
      <exit code="0" />
      <exit code="128" />
    </upgrade>

    <upgrade include='install' />

    <remove cmd='taskkill /F /IM thunderbird.exe'>
      <exit code="0" />
      <exit code="128" />
    </remove>
    <remove cmd='%WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Au_.exe' />
    <remove cmd='"%programfiles(x86)%\Mozilla Thunderbird\uninstall\helper.exe" /S' />
    <remove cmd='%WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Au_.exe' />
    <remove cmd='reg delete HKLM\SOFTWARE\Policies\Mozilla\Thunderbird /v DisableAppUpdate /f'/>
  </package>

</packages:packages>
