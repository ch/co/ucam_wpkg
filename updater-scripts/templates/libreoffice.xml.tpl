<?xml version="1.0" encoding="UTF-8"?>
<packages:packages xmlns:packages="http://www.wpkg.org/packages">

  <package id="libreoffice" name="LibreOffice" revision="%version%.1" priority="73" reboot="false">
    <variable name="version" value="${VERSION}" />
    <check type="uninstall" condition="versiongreaterorequal" path="LibreOffice .+" value="%version%" />

    <install cmd='msiexec /qn /i "%WPKGSOFTWARE%\libreoffice\LibreOffice_%version%_Win_x64.msi" ADDLOCAL=ALL ALLUSERS=1 CREATEDESKTOPLINK=0 RebootYesNo=No ISCHECKFORPRODUCTUPDATES=0' />

    <upgrade include="install" />

    <remove cmd='msiexec /qn /x $UNINSTALL RebootYesNo=No' />
  </package>

</packages:packages>
