<?xml version="1.0" encoding="UTF-8"?>

<packages:packages xmlns:packages="http://www.wpkg.org/packages">

  <package id="filezilla" name="FileZilla" revision="%version%.1" priority="0"	reboot="false">
    <variable name="version" value="$VERSION" />
    <check type="uninstall" condition="exists" path="FileZilla %version%" />

    <install cmd='%WPKGSOFTWARE%\filezilla\FileZilla_%version%_win64-setup.exe /S' />

    <upgrade include="remove" />
    <upgrade include="install" />

    <remove cmd='%WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Un_A.exe' />
    <remove cmd='"%PROGRAMFILES%\FileZilla FTP Client\uninstall.exe" /S' />
    <remove cmd='%WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Un_A.exe' />
  </package>

</packages:packages>
