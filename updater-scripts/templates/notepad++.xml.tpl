<?xml version="1.0" encoding="UTF-8"?>
<packages:packages xmlns:packages="http://www.wpkg.org/packages">

  <package id="notepad++" name="Notepad++" revision="%version%.1" reboot="false" priority="90">
    <variable name="version" value="${VERSION}" />

    <!-- 64bit version exists as of 7.1, but most of the plugins aren't available. So stick with 32bit until they sort that -->
    <check type="uninstall" condition="versiongreaterorequal" path="Notepad++ (32-bit x86)" value="%version%" />

    <!-- remove plug-ins to prevent installer hanging -->
    <!-- Exit code of "1" can occur if there are no .dll files in the plugins dir. This happens if a previous installation has been uninstalled,
     since the uninstaller fails to remove the plugins folder. -->
    <install cmd='cmd /c del /f /q "%ProgramFileswpkg%\Notepad++\plugins\*.dll"'><exit code="1"/></install>
    <install cmd="%WPKGSOFTWARE%\notepad++\npp.%version%.Installer.exe /S" />

    <!-- remove plug-ins to prevent installer hanging -->
    <upgrade cmd='cmd /c del /f /q "%ProgramFileswpkg%\Notepad++\plugins\*.dll"' ><exit code="1"/></upgrade>
    <!-- kill any running notepad++.exe: otherwise, the upgrade appears to succeed but
       we're actually left with the old binary -->
    <upgrade cmd='taskkill /F /IM notepad++.exe /FI "STATUS eq RUNNING"' />
    <upgrade cmd="%WPKGSOFTWARE%\Notepad++\npp.%version%.Installer.exe /S" />

    <remove cmd='cmd /c del /f /q "%ProgramFileswpkg%\Notepad++\plugins\*.dll"' ><exit code="any"/></remove>
    <remove cmd='%WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Un_A.exe' />
    <remove cmd='"%ProgramFiles(x86)%\Notepad++\uninstall.exe" /S' />
    <remove cmd='%WPKGSOFTWARE%\wpkg\tools\waitforprocess.cmd Un_A.exe' />
  </package>

</packages:packages>
