<?xml version="1.0" encoding="UTF-8"?>
<!--  vim: set softtabstop=3: -->
<packages:packages xmlns:packages="http://www.wpkg.org/packages">
  <package id="vscode" name="Visual Studio Code" revision="%version%.1" priority="0" reboot="false">
    <variable name="version" value="${VERSION}" />

    <check type="uninstall" path="Microsoft Visual Studio Code" condition="versiongreaterorequal" value="%version%" />

    <!-- the silent installer defaults to launching the software unless we specify the extra MERGETASKS flag:
         https://stackoverflow.com/questions/42582230/how-to-install-visual-studio-code-silently-without-auto-open-when-installation
         https://github.com/Microsoft/vscode/issues/19319 -->

    <install cmd='%WPKGSOFTWARE%\microsoft.com\vscode\vscodesetup-x64-%version%.exe /VERYSILENT /MERGETASKS=!runcode ' />

    <upgrade include='remove' />
    <upgrade include='install' />

    <remove cmd='"%programfiles%\Microsoft VS Code\unins000.exe" /VERYSILENT' />

  </package>
</packages:packages>
