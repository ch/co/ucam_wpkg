<?xml version="1.0" encoding="UTF-8"?>
<packages:packages xmlns:packages="http://www.wpkg.org/packages">

<package  id="putty"  name="PuTTY"  revision="%version%.1"  priority="50"  reboot="false">
	<variable name="version" value="$VERSION" />

  <check type="uninstall" condition="exists" path="PuTTY release %version% (64-bit)" />

  <install cmd="msiexec /qn /l* %WPKGLOGDIR%\puttyrelease-%version%-log /i %WPKGSOFTWARE%\putty\putty-64bit-%version%-installer.msi">
    <exit code="3010" reboot="false" />
  </install>

	<upgrade cmd='taskkill /f /im putty.exe /im pageant.exe'>
		<exit code='0' />
		<exit code='1' />
		<exit code='128' />
	</upgrade>
  <!-- NB switched from 32 to 64 bit installer as of 0.73 upgrade, hence the need
       for the explicit uninstallation (the old 32bit version is left behind otherwise).
       If we're ever happy that all 0.73 installs have gone, we can remove the uninstallation
       upgrade step -->
  <upgrade cmd='MsiExec /qn /x {9A5E8BB6-AECA-42FB-8E76-DC8EA546AF2A}'><exit code="1605" /></upgrade> <!-- uninstall 0.73 -->
  <upgrade include='install' />

	<remove cmd='taskkill /f /im putty.exe /im pageant.exe'>
		<exit code='0' />
		<exit code='1' />
		<exit code='128' />
	</remove>
  <remove cmd="msiexec /qn /x $PRODUCTCODE">
    <!-- 1605 - already removed -->
    <exit code="1605" />
  </remove>

</package>

</packages:packages>
