<?xml version="1.0" encoding="UTF-8"?>
<!--  vim: set softtabstop=3: -->
<packages:packages xmlns:packages="http://www.wpkg.org/packages">

  <package id="pgadmin4" name="pgAdmin 4" revision="%version%.1" priority="50" reboot="false">

  <variable name="majorversion" value="${MAJORVERSION}" />
  <variable name="minorversion" value="${MINORVERSION}" />
  <variable name="version" value="%majorversion%.%minorversion%" />
  <variable name="installationdir" value="%programfiles%\pgadmin4" />

  <depends package-id="vs2013x86runtime" />

  <check type="uninstall" condition="exists" path="pgAdmin 4 version %version%" />

  <install cmd='%wpkgsoftware%\pgadmin\pgadmin4-%version%-x64.exe /SP- /VERYSILENT /SUPPRESSMSGBOXES /NORESTART /ALLUSERS /DIR="%installationdir%"' />
  <install cmd='cmd /c copy "%WPKGSHAREBASE%\%WPKGINSTITUTION%-config\pgadmin\config_local.py" "%installationdir%\web\"'>
    <condition>
      <check type="file" condition="exists" path="%WPKGSHAREBASE%\%WPKGINSTITUTION%-config\pgadmin\config_local.py" />
    </condition>
  </install>

  <upgrade include='remove' />
  <upgrade include='install' />

  <!-- legacy remove cmds ; will not be needed once all clients are past pgadmin4 v4 -->
  <remove cmd='cmd /c if exist "%programfileswpkg%\pgadmin 4\v4\web\config_local.py" del "%programfileswpkg%\pgadmin 4\v4\web\config_local.py"'>
    <exit code='any' />
  </remove>
  <remove cmd='"%programfileswpkg%\pgadmin 4\v4\unins000.exe" /VERYSILENT /SP- /NORESTART'>
    <condition>
      <check type="file" condition="exists" path="%programfileswpkg%\pgadmin 4\v4\unins000.exe" />
    </condition>
  </remove>

  <!-- remove our config file: the uninstaller does not, and thus also leaves the directory tree behind -->
  <remove cmd='cmd /c if exist "%programfiles%\pgadmin4\web\config_local.py" del "%programfileswpkg%\pgadmin4\web\config_local.py"'>
    <exit code='any'/>
  </remove>
  <remove cmd='"%programfiles%\pgadmin4\unins000.exe" /VERYSILENT /SP- /NORESTART'>
    <condition>
      <check type="file" condition="exists" path="%programfiles%\pgadmin4\unins000.exe" />
    </condition>
  </remove>

  </package>
  
</packages:packages>
