<packages:packages xmlns:packages="http://www.wpkg.org/packages">
  <package id="zoom" name="Zoom" revision="%version%.1" reboot="false">
    <!-- Zoom have an odd approach to version numbers. This version does not seem to match
         anything in the GUI, so check the MSI -->
    <variable name="version" value="${VERSION}" />

    <check type="uninstall" condition="versiongreaterorequal" path="Zoom Workplace (32-bit)" value="%version%" />

    <!-- get the MSI from https://www.zoom.us/client/latest/ZoomInstallerFull.msi
         MSI documentation at https://support.zoom.us/hc/en-us/articles/201362163-Mass-Installation-and-Configuration-for-Windows -->
    <install cmd='msiexec /qn /l* %WPKGLOGDIR%\zoom.log /i %WPKGSOFTWARE%\zoom\%version%\ZoomInstallerFull.msi'>
      <exit code="3010" reboot="false" />
    </install>

    <upgrade include="install" />

    <remove cmd="msiexec /qn /x ${PRODUCTCODE}">
      <exit code="1605" />
    </remove>
  </package>
</packages:packages>
