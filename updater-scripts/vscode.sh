#!/bin/bash

set -u

XMLFILE=vscode.xml

. common.inc.sh

REMOTEDIR=software/microsoft.com/vscode

# curl -I https://go.microsoft.com/fwlink/?Linkid=852157 
# I'm presuming for now that the Location for that link doesn't change!

DLFILE=$(curl -s -I https://update.code.visualstudio.com/latest/win32-x64/stable | grep -i ^Location | cut -d ' ' -f 2 | tr -d '\r')
VERSION=$(basename $DLFILE | sed "s/^VSCodeSetup-x64-//" | sed "s/\.exe$//")
export VERSION

TDIR=$(mktemp -d)
OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)

echo New version $VERSION was $OVERSION
if [ $VERSION != $OVERSION ] ; then
 (cd $TDIR &&  wget --content-disposition $DLFILE)
 FILENAME=$(ls -1 $TDIR)
 envsubst < $TEMPLATE >$PKGDIR/$XMLFILE.new
 if ! cmp $PKGDIR/$XMLFILE.new $PKGDIR/vscode.xml ; then
  cp $PKGDIR/$XMLFILE.new $PKGDIR/vscode.xml
  smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put $TDIR/$FILENAME $FILENAME"
 fi
 rm $PKGDIR/$XMLFILE.new
fi
[ -d "$TDIR" ] && rm -Rf "$TDIR"
