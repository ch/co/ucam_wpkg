#!/bin/bash
set -u

XMLFILE=git.xml

. common.inc.sh

TEMPLATE=$TEMPLATEDIR/$XMLFILE.tpl
REMOTEDIR=software/git
DESCURL=https://gitforwindows.org/
TDIR=$(mktemp -d)

VERSION=$(wget -q --no-check-certificate $DESCURL -O - |  grep '<div class="version">'| sed "s/.*Version \([[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+\).*/\1/")
export VERSION

FILENAME=Git-${VERSION}-64-bit.exe
DOWNLOADURL=https://github.com/git-for-windows/git/releases/download/v${VERSION}.windows.1/${FILENAME}

OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)

echo New version $VERSION was $OVERSION
if [ $VERSION != $OVERSION ] ; then
 echo downloading
 wget -q --no-check-certificate $DOWNLOADURL -O $TDIR/$FILENAME
 smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put $TDIR/$FILENAME $FILENAME"
 envsubst < $TEMPLATE > $PKGDIR/${XMLFILE}.new
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi
rm -Rf $TDIR
