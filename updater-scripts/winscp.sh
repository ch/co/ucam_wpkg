#!/bin/bash
set -u

XMLFILE=winscp.xml

. common.inc.sh

TEMPLATE=$TEMPLATEDIR/$XMLFILE.tpl
REMOTEDIR=software/winscp
DESCURL=https://winscp.net/eng/download.php
TDIR=$(mktemp -d)

# Yuk, sucky parsing of HTML via grep!
# looking for .... Download <strong>WinSCP</strong> 5.13.2 (x.xxMB)
VERSION=$(wget -q --no-check-certificate $DESCURL -O -  | grep "Download <strong>" | sed "s#.*Download <strong>WinSCP</strong> \(.*\) (.*#\1#")
export VERSION

FILENAME=WinSCP-${VERSION}-Setup.exe
DOWNLOADURL=https://winscp.net/download/WinSCP-${VERSION}-Setup.exe

OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)

echo New version $VERSION was $OVERSION
if [ $VERSION != $OVERSION ] ; then
 echo downloading
 wget -q --no-check-certificate $DOWNLOADURL -O $TDIR/$FILENAME
 smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put $TDIR/$FILENAME $FILENAME"
 envsubst < $TEMPLATE > $PKGDIR/${XMLFILE}.new
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi
rm -Rf $TDIR
