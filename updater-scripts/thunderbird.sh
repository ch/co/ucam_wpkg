#!/bin/bash

set -u

XMLFILE=thunderbird.xml

. common.inc.sh

REMOTEDIR=software/thunderbird

TDIR=$(mktemp -d)

DESCURL="https://www.thunderbird.net/en-US/download/"

# FILEVERSION might have a "esr" suffix. We keep the filename as-is, but then
# need to remove "esr" for use in the wpkg package version.
FILEVERSION=$(curl -s "https://www.thunderbird.net/en-US/download/" -o - | grep "os=win" | grep -v "<li>" | head -n1 | sed "s/.*thunderbird-\(.*\)-SSL.*/\1/")
VERSION=${FILEVERSION%%esr}
PREFSVERSION=$(echo $VERSION | sed "s/\([^\.]*\).*/\1/")

export VERSION
export FILEVERSION
export PREFSVERSION

DLURL="https://download.mozilla.org/?product=thunderbird-$FILEVERSION-SSL&os=win&lang=en-GB"
FILENAME="Thunderbird Setup $FILEVERSION.exe"
OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)

echo New version $VERSION was $OVERSION
if [ $VERSION != $OVERSION ] ; then
 wget -q --no-check-certificate $DLURL -O "$TDIR/$FILENAME"
 envsubst< $TEMPLATE >$PKGDIR/${XMLFILE}.new
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
  echo Package updated
  echo check for WPKGIMSTITUTION-config\\thunderbird\\WPKGINSTIUTION-prefs.js-$PREFSVERSION
  smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put \"$TDIR/$FILENAME\" \"$FILENAME\""
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi
rm -Rf $TDIR
