#!/bin/bash

set -u

XMLFILE=libreoffice.xml

. common.inc.sh

REMOTEDIR=software/libreoffice

DESCURL="http://www.libreoffice.org/download/download/"
TDIR=$(mktemp -d)

VERSION=$(wget -q -O - $DESCURL | grep "type=win-x86_64" | head -n1 | sed -E 's#.*version=([[:digit:]\.]+)[^[:digit:]\.].*#\1#')
export VERSION

FILENAME="LibreOffice_${VERSION}_Win_x64.msi"
OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)

DLURL="https://download.documentfoundation.org/libreoffice/stable/${VERSION}/win/x86_64/LibreOffice_${VERSION}_Win_x86-64.msi"

echo New version $VERSION was $OVERSION
if [ $VERSION != $OVERSION ] ; then

 wget -q --no-check-certificate $DLURL -O "$TDIR/$FILENAME"
 UNINSTALL=`msi-productcode "$TDIR/$FILENAME"`
 export UNINSTALL
 envsubst < $TEMPLATE > $PKGDIR/${XMLFILE}.new
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
  echo Package updated
  smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put $TDIR/$FILENAME $FILENAME"
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi

[ -d "$TDIR" ] && rm -Rf "$TDIR"
