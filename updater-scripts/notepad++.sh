#!/bin/bash

set -u

XMLFILE=notepad++.xml

. common.inc.sh

REMOTEDIR=software/notepad++
DESCURL=https://notepad-plus-plus.org/downloads/
TDIR=$(mktemp -d)

# Yuk, sucky parsing of HTML via grep!
VERSION=$(wget -q --no-check-certificate $DESCURL -O - | grep Current  | sed "s#.*Current Version \(.*\)</strong>.*#\1#")
export VERSION
MAJORVERSION=$(echo $VERSION | sed "s/\([^\.]*\).*/\1/")

DLURL="http://download.notepad-plus-plus.org/repository/$MAJORVERSION.x/$VERSION/npp.$VERSION.Installer.exe"
FILENAME="npp.$VERSION.Installer.exe"
OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)

echo New version $VERSION was $OVERSION
if [ $VERSION != $OVERSION ] ; then
 echo downloading $DLURL
 wget -q --no-check-certificate $DLURL -O "$TDIR/$FILENAME"
 envsubst < $TEMPLATE > $PKGDIR/${XMLFILE}.new
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
  echo Package updated
  smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put $TDIR/$FILENAME $FILENAME"
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi
rm -Rf $TDIR
