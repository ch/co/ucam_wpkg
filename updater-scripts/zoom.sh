#!/bin/bash

set -u
XMLFILE=zoom.xml

. common.inc.sh

REMOTEDIR=software/zoom
TDIR=$(mktemp -d)

DLURL=https://www.zoom.us/client/latest/ZoomInstallerFull.msi

FILENAME=ZoomInstallerFull.msi

wget -q --no-check-certificate $DLURL -O "$TDIR/$FILENAME"

MAJOR_MINOR=$(msiinfo export $TDIR/$FILENAME _SummaryInformation | awk '$1==6 {print $2}' |cut -d '.' -f 1,2)
BUILD=$(msiinfo export $TDIR/$FILENAME _SummaryInformation | awk '$1==6 {print $3}' | sed 's#(\(.*\))#\1#' | tr -d '\r')
PRODUCTCODE=$(msi-productcode $TDIR/$FILENAME | tr -d ' ')

VERSION=${MAJOR_MINOR}.${BUILD}

export VERSION
export PRODUCTCODE

OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)
echo New version $VERSION was $OVERSION

if [ $VERSION != $OVERSION ] ; then
 envsubst < $TEMPLATE > $PKGDIR/${XMLFILE}.new
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
  echo Package updated
  smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "mkdir $REMOTEDIR/$VERSION; cd $REMOTEDIR/$VERSION; put $TDIR/$FILENAME $FILENAME"
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi
rm -Rf $TDIR
