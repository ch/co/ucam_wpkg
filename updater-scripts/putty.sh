#!/bin/bash

set -u
XMLFILE=putty.xml

. common.inc.sh

REMOTEDIR=software/putty
TDIR=$(mktemp -d)

DESCURL=https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
VERSION=$(wget -q --no-check-certificate $DESCURL -O - | grep "^<h1" | sed 's/<h1.*>Download PuTTY: latest release (\(.*\))<\/h1>/\1/')
DLURL="https://the.earth.li/~sgtatham/putty/latest/w64/putty-64bit-${VERSION}-installer.msi"

FILENAME="putty-64bit-${VERSION}-installer.msi"

wget -q --no-check-certificate $DLURL -O "$TDIR/$FILENAME"

PRODUCTCODE=$(msi-productcode $TDIR/$FILENAME | tr -d ' ')

export VERSION
export PRODUCTCODE

OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)
echo New version $VERSION was $OVERSION

if [ $VERSION != $OVERSION ] ; then
 envsubst < $TEMPLATE > $PKGDIR/${XMLFILE}.new
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
  echo Package updated
  smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put $TDIR/$FILENAME $FILENAME"
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi
rm -Rf $TDIR
