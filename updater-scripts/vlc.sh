#!/bin/bash
set -u

XMLFILE=vlcclient.xml

. common.inc.sh

TEMPLATE=$TEMPLATEDIR/$XMLFILE.tpl
REMOTEDIR=software/vlc
DESCURL=https://www.videolan.org/
TDIR=$(mktemp -d)

# NB page lists win32 download link by default but we assume we can downloda
# the win64 version in a few lines time...
VERSION=$( wget -q $DESCURL -O - | grep "'downloadButton2'" | sed "s#.*get.videolan.org/vlc/\(.*\)/win32/.*#\1#")
export VERSION

FILENAME=vlc-${VERSION}-win64.exe
DOWNLOADURL=https://get.videolan.org/vlc/$VERSION/win64/vlc-$VERSION-win64.exe

OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)

echo New version $VERSION was $OVERSION
if [ $VERSION != $OVERSION ] ; then
 echo downloading
 wget -q --no-check-certificate $DOWNLOADURL -O $TDIR/$FILENAME
 smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put $TDIR/$FILENAME $FILENAME"
 envsubst < $TEMPLATE > $PKGDIR/${XMLFILE}.new
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi
rm -Rf $TDIR
