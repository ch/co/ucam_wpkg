#!/bin/bash
set -u
XMLFILE=pgadmin4.xml

. common.inc.sh

TEMPLATE=$TEMPLATEDIR/$XMLFILE.tpl
REMOTEDIR=software/pgadmin
DESCURL="https://www.pgadmin.org/download/pgadmin-4-windows/"
TDIR=$(mktemp -d)
COOKIEJAR=$(mktemp)

VERSION=$(wget -q --no-check-certificate ${DESCURL} -O - | grep fa-download | grep "<li>" | head -n1  | sed "s#.* v\(.*\)</a>.*#\1#")
MAJORVERSION=$(echo $VERSION | cut -d '.' -f 1)
MINORVERSION=$(echo $VERSION | cut -d '.' -f 2)
export VERSION
export MAJORVERSION
export MINORVERSION

FILENAME="pgadmin4-${VERSION}-x64.exe"

OLD_MAJORVERSION=$(xmlstarlet sel -t -m '//variable[@name="majorversion"]' -v '@value' $PKGDIR/$XMLFILE)
OLD_MINORVERSION=$(xmlstarlet sel -t -m '//variable[@name="minorversion"]' -v '@value' $PKGDIR/$XMLFILE)
OVERSION="${OLD_MAJORVERSION}.${OLD_MINORVERSION}"

DOWNLOADURL="https://ftp.postgresql.org/pub/pgadmin/pgadmin4/v${VERSION}/windows/${FILENAME}"

echo New version $VERSION was $OVERSION
if [ $VERSION != $OVERSION ] ; then
 echo downloading
 wget -q --no-check-certificate $DOWNLOADURL -O $TDIR/$FILENAME
 smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put $TDIR/$FILENAME $FILENAME"
 envsubst < $TEMPLATE > $PKGDIR/${XMLFILE}.new
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi
[ -d "$TDIR" ] && rm -Rf $TDIR
[ -f "$COOKIEJAR" ] && rm -f $COOKIEJAR
