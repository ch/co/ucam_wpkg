#!/bin/bash
set -u
XMLFILE=filezilla.xml

. common.inc.sh

TEMPLATE=$TEMPLATEDIR/$XMLFILE.tpl
REMOTEDIR=software/filezilla
DESCURL="https://filezilla-project.org/download.php?show_all=1"
TDIR=$(mktemp -d)
COOKIEJAR=$(mktemp)

VERSION=$(wget -q --no-check-certificate ${DESCURL} -O - | grep "^<p>The latest stable version of FileZilla Client is" | sed 's/.* \([0-9\.]\+\)<\/p>$/\1/')
export VERSION

FILENAME="FileZilla_${VERSION}_win64-setup.exe"

OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)

# need to replace ampersands by HTML entity for xmlstarlet, then put them back to plain & for wget
DOWNLOADURL=$(curl -A 'Firefox' -c $COOKIEJAR -s -o - "https://filezilla-project.org/download.php?show_all=1" | grep win64-setup.exe | grep https | sed 's/&/\&amp;/g' | xmlstarlet sel -t -v '//a/@href' | sed 's/&amp;/\&/g')

echo New version $VERSION was $OVERSION
if [ $VERSION != $OVERSION ] ; then
 echo downloading
 wget -q --no-check-certificate $DOWNLOADURL -O $TDIR/$FILENAME
 smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put $TDIR/$FILENAME $FILENAME"
 envsubst < $TEMPLATE > $PKGDIR/${XMLFILE}.new
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi
[ -d "$TDIR" ] && rm -Rf $TDIR
[ -f "$COOKIEJAR" ] && rm -f $COOKIEJAR
