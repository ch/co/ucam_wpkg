#!/bin/bash

set -u

XMLFILE=wireshark.xml

. common.inc.sh

REMOTEDIR=software/wireshark
DESCURL=https://www.wireshark.org/download.html
TDIR=$(mktemp -d)

# Yuk, sucky parsing of HTML via grep!
VERSION=$(wget --no-check-certificate $DESCURL -q -O - | grep 'Stable Release' | sed 's#.*<summary>Stable Release: \([[:digit:]\.]*\) </summary>.*#\1#')
export VERSION

FILENAME="Wireshark-$VERSION-x64.exe"
DLURL="https://1.na.dl.wireshark.org/win64/$FILENAME"
OVERSION=$(xmlstarlet sel -t -m '//variable[@name="version"]' -v '@value' $PKGDIR/$XMLFILE)

echo New version $VERSION was $OVERSION
if [ $VERSION != $OVERSION ] ; then
 wget -q --no-check-certificate $DLURL -O "$TDIR/$FILENAME"
 envsubst < $TEMPLATE > $PKGDIR/${XMLFILE}.new
 smbclient --use-krb5-ccache=/tmp/krb5cc_$(id -u) $SHARE --command "cd $REMOTEDIR; put $TDIR/$FILENAME $FILENAME"
 if ! cmp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE ; then
  cp $PKGDIR/${XMLFILE}.new $PKGDIR/$XMLFILE
  echo Package updated
 fi
 rm $PKGDIR/${XMLFILE}.new
else
 echo Package not updated.
fi
rm -Rf $TDIR
