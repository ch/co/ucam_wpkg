#!/bin/bash

if [ -z "$XMLFILE" ] ; then
  echo please set XMLFILE before sourcing me
  exit 1
fi

if ! klist -s ; then
  echo no kerberos ticked found. Please give your Admitto password when prompted.
  kinit
fi

SCRIPTDIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
REPOBASE=$(dirname $SCRIPTDIR)
PKGDIR=$REPOBASE/packages
TEMPLATEDIR=$SCRIPTDIR/templates
TEMPLATE=$TEMPLATEDIR/$XMLFILE.tpl

SHARE=//ifs.ch.private.cam.ac.uk/deploy
